package com.example.contact_demo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.*;
import java.text.StringCharacterIterator;

public class Main extends Activity
{
    private static final int SELECT_PHOTO = 100;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        this.initUI();
        this.loadData();

        Button button = (Button)findViewById(R.id.btn_photo);
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        CheckBox cb = (CheckBox)findViewById(R.id.cbSave);

        if(cb.isChecked())
            this.saveData();

        Log.v("Main", "onStop");
    }

    /** Button Listener */
    public void onBtnLoad(View view) {
        Log.v("Main", "onBtnLoad");
        this.loadData();
    }

    /** Callback for startActivity */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();

                    Bitmap yourSelectedImage = null;
                    try{
                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    }
                    catch(Exception e){

                    }

                    ImageView photo = (ImageView)findViewById(R.id.photo);
                    photo.setImageBitmap(yourSelectedImage);
                }
        }
    }

    /** add action bar */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /** Handling click events */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                this.saveData();
                return true;
            case R.id.delete:
                this.deleteAll();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Initialize UI */
    protected void initUI(){
        EditText edtName = (EditText)findViewById(R.id.edtName);
        EditText edtEmail = (EditText)findViewById(R.id.edtEmail);
        EditText edtPhone = (EditText)findViewById(R.id.edtPhone);
        ImageView photo = (ImageView)findViewById(R.id.photo);

        edtName.setMaxWidth(300);
        edtEmail.setMaxWidth(300);
        edtPhone.setMaxWidth(300);

        photo.setMaxHeight(100);
        photo.setMaxWidth(100);

        edtName.setText("");
        edtEmail.setText("");
        edtPhone.setText("");

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
        photo.setImageBitmap(bitmap);

    }

    /** load data from file */
    protected void loadData(){
        EditText edtName = (EditText)findViewById(R.id.edtName);
        EditText edtEmail = (EditText)findViewById(R.id.edtEmail);
        EditText edtPhone = (EditText)findViewById(R.id.edtPhone);
        ImageView photo = (ImageView)findViewById(R.id.photo);
        CheckBox cb = (CheckBox)findViewById(R.id.cbSave);

        String data[] = this.readContactFromFile();

        edtName.setText(data[0]);
        edtEmail.setText(data[1]);
        edtPhone.setText(data[2]);
        if( data[3] != null && data[3].equals("checked")){
            cb.setChecked(true);
        }


        Bitmap bitmap = this.readPhotoFromFile();
        if(bitmap != null){
            photo.setImageBitmap(bitmap);
        }
    }

    /** load data to file */
    protected void saveData(){
        EditText edtName = (EditText)findViewById(R.id.edtName);
        EditText edtEmail = (EditText)findViewById(R.id.edtEmail);
        EditText edtPhone = (EditText)findViewById(R.id.edtPhone);
        ImageView photo = (ImageView)findViewById(R.id.photo);
        CheckBox cb = (CheckBox)findViewById(R.id.cbSave);

        String name = edtName.getText().toString();
        String email = edtEmail.getText().toString();
        String phone = edtPhone.getText().toString();
        String checked = "unchecked";
        if( cb.isChecked())
            checked = "checked";

        this.saveContactToFile(name, email, phone, checked);

        photo.buildDrawingCache();
        if( photo.getDrawingCache() != null)
            this.savePhotoToFile(photo.getDrawingCache());
    }

    /** save text value to file */
    protected void saveContactToFile(String name, String email, String phone, String checked){
        Log.v("Main", "saveContactToFile");

        String filename = "contact";

        String data = name + "\n"
                      + email +"\n"
                      + phone + "\n"
                      + checked;

        FileOutputStream outputStream;

        try {

            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();

            Log.v("Main", e.toString());
        }
    }

    /** save image to file */
    protected void savePhotoToFile(Bitmap photo){
        Log.v("Main", "savePhotoToFile");

        String filename = "photo";
        FileOutputStream outputStream;

        try {

            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            photo.compress(Bitmap.CompressFormat.PNG, 90, outputStream);

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();

            Log.v("Main", "savePhotoToFile exception");
            Log.v("Main", e.toString());
        }
    }

    /** read text data from file */
    protected String[] readContactFromFile(){
        String filename = "contact";
        FileInputStream inputStream;
        StringBuilder sb = null;
        String data[] = new String[4];

        try {
            inputStream = openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            sb = new StringBuilder();

            String line = null;

            int i=0;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);

                data[i] = line;

                i++;
            }

            inputStreamReader.close();
            inputStream.close();

            Log.v("Main", sb.toString());
        } catch (Exception e) {
            Log.v("Main", "readContactFromFile exception");
            Log.v("Main", e.toString());

            e.printStackTrace();

            data[0] = "";
            data[1] = "";
            data[2] = "";
            data[3] = "";
        }
        return data;
    }

    /** read image from file */
    protected Bitmap readPhotoFromFile(){
        Log.v("Main", "readPhotoFromFile");

        Bitmap photo = null;
        try{
            String filename=new File(getFilesDir(), "photo").getAbsolutePath();

            photo = BitmapFactory.decodeFile(filename);
            Log.v("Main", photo.toString());

        } catch (Exception e) {
            Log.v("Main", "readPhotoFromFile exception");
            Log.v("Main", e.toString());
            e.printStackTrace();
        }
        return photo;
    }

    /** delete files in app */
    protected void deleteAll(){
        Context context = getApplicationContext();
        context.deleteFile("photo");
        context.deleteFile("contact");

        this.initUI();
    }
}

